from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from django.contrib.auth.models import User
from .models import Post,Slide
from django.shortcuts import get_object_or_404
from django.utils import timezone
class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()
    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        pk = text_data_json['pk']
        slide = get_object_or_404(Slide,pk=pk)
        user = User.objects.first()
        rep_max = slide.max_rep_num
        post_at = timezone.datetime.now()
        post = Post.objects.create(
            message=message,
            slide=slide,
            post_by=user,
            post_at=post_at,
            rep_num=rep_max
        )
        slide.max_rep_num+=1
        slide.save()
        if(post_at.hour < 10):
            hour = '0' + str(post_at.hour)
        else:
            hour = str(post_at.hour)
        if(post_at.minute < 10):
            minute = '0' + str(post_at.minute)
        else:
            minute = str(post_at.minute)
        time = str(post_at.year)+'年'+str(post_at.month)+'月'+str(post_at.day)+'日'+hour+':'+minute
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            { 'type': 'chat_message', 'message': message,'rep':rep_max,'user':user.username,'time':time}
        )
    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        rep = event['rep']
        user = event['user']
        time = event['time']
        # Send message to WebSocket
        self.send(text_data=json.dumps({'message': message,'rep':rep,'user':user,'time':time}))