from django.urls import path
from . import views

app_name='prog'

urlpatterns = [
    path('', views.home, name='home'),
    path('<str:language>/',views.index,name='index'),
    path('<str:language>/<int:pk>/',views.learn,name='learn'),
    path('setting/user/',views.setting,name='setting'),
    path('edit/',views.edit,name='edit'),
]
