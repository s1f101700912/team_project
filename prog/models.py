from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class ProgramingLanguage(models.Model):
    language = models.CharField(max_length=20)
    offical_text = models.TextField(max_length=1000)
    def __str__(self):
        return self.language

class Slide(models.Model):
    plograming_language = models.ForeignKey(ProgramingLanguage,on_delete=models.CASCADE,related_name='slide')
    slide_url = models.URLField(max_length=200)
    title = models.CharField(max_length=50)
    explain = models.TextField(max_length=1000)
    max_rep_num = models.IntegerField(default=1)
    def __str__(self):
        return self.title

class Post(models.Model):
    message = models.TextField(max_length=4000)
    slide = models.ForeignKey(Slide,on_delete=models.CASCADE,related_name='post')
    post_at = models.DateTimeField(null=True)
    post_by = models.ForeignKey(User,on_delete=models.CASCADE,related_name='post')
    rep_num = models.IntegerField(default=0)
    def __str__(self):
        return self.message
