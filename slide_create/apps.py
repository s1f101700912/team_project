from django.apps import AppConfig


class SlideCreateConfig(AppConfig):
    name = 'slide_create'
