from django import forms
from prog.models import Slide,ProgramingLanguage

class NewSlideForm(forms.ModelForm):
    class Meta:
        model = Slide
        fields = ['title', 'slide_url', 'explain']

class NewLangForm(forms.ModelForm):
    class Meta:
        model = ProgramingLanguage
        fields = ['language', 'offical_text']