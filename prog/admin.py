from django.contrib import admin
from .models import ProgramingLanguage,Slide,Post
# Register your models here.
admin.site.register(ProgramingLanguage)
admin.site.register(Slide)
admin.site.register(Post)