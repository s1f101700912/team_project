from django.urls import path
from . import views 

app_name = 'slide_create'

urlpatterns = [
    path('',views.list,name='list'),
    path('create_lang/',views.create_lang,name='create_lang'),
    path('<int:pk>/',views.lang,name='lang'),
    path('<int:pk>/new/', views.new_slide, name='new_slide'),
]