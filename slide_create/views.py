from django.contrib.auth.decorators import login_required
from django.shortcuts import render,redirect,get_object_or_404
from django.urls import reverse
from prog.models import ProgramingLanguage,Slide
from django.contrib.auth.models import User
from .forms import NewSlideForm,NewLangForm

def list(request):
    prog = ProgramingLanguage.objects.all()
    return render(request,'slide_create/list.html',{'prog':prog})

def create_lang(request):
    if request.method == 'POST':
        form = NewLangForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.save()
            return redirect('slide_create:list')
    else:
        form = NewLangForm()
    return render(request,'slide_create/create_lang.html',{'form':form})
    

def lang(request,pk):
    lang = get_object_or_404(ProgramingLanguage,pk=pk)
    return render(request,'slide_create/lang.html',{'lang':lang})

def new_slide(request,pk):
    lang = get_object_or_404(ProgramingLanguage,pk=pk)

    if request.method == 'POST':
        form = NewSlideForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.plograming_language = lang
            form.save()
            return redirect('slide_create:lang',pk=lang.pk)
    else:
        form = NewSlideForm()
    return render(request,'slide_create/new_slide.html',{'lang':lang,'form':form})