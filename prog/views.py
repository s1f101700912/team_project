from django.contrib.auth.decorators import login_required
from django.shortcuts import render,get_object_or_404
from django.urls import reverse
from .models import ProgramingLanguage,Slide
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
import json

@login_required
def home(request):
    lang = ProgramingLanguage.objects.all()
    return render(request, 'prog/home.html', {'lang':lang})

@login_required
def index(request,language):
    lang = ProgramingLanguage.objects.all()
    language = get_object_or_404(ProgramingLanguage,language=language)
    return render(request, 'prog/index.html', {'lang':lang,'language':language})

@login_required
def learn(request,language,pk):
    lang = ProgramingLanguage.objects.all()
    slide = get_object_or_404(Slide,pk=pk)
    room_name = language + str(pk)
    content = {
        'room_name_json': mark_safe(json.dumps(room_name)),
        'lang':lang,
        'slide':slide
        }
    return render(request, 'prog/learn.html', content)

def edit(request):
    user = User.objects.first()
    lang = ProgramingLanguage.objects.all()
    content = {
        'lang':lang,
        'slide':slide,
        'user':user,
        }
    return render(request, 'prog/edit.html', content)

@login_required
def setting(request):
    lang = ProgramingLanguage.objects.all()
    return render(request, 'prog/setting.html', {'lang':lang})

